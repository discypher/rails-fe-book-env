# Dev Environment for Modern Front End Development for Rails, Second Edition

## Install Docker
Install Docker Desktop for your OS. https://www.docker.com/products/docker-desktop/


## Need to update the following files
In the code from the website, in `chapter_01/02` you will need to update the following files:  

- `config/database.yml`  
We need to add `host` to reflect what the posgres container will be available as in the Docker network. Also need to add username and password for the user.  
```
default: &default
  adapter: postgresql
  encoding: unicode
  pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>
  host: pg
development:
  <<: *default
  database: north_by_seven_development
  username: north_by_seven
  password: 'secret'
test:
  <<: *default
  database: north_by_seven_test
  username: north_by_seven
...
```

- `Procfile`  
Because we're running in Docker, we need to bind to '0.0.0.0'. 
```
web: bin/rails server -p 3000 -b 0.0.0.0
...
```

- `bin/setup`  
Because Alpine Linux does not have bash, we need to use ash.  
```
#!/usr/bin/env ash
```

- `config/environments/development.rb`  
We need to allow the Docker network IPs to have access to the web console.  
```
Rails.application.configure do
  config.web_console.whitelisted_ips = '172.0.0.0/8'
   ...
end
```

And finally, you'll need to add a `.env` file to this project directory with the path to where you downloaded the code for the book, and point at `chapter_01/02`, which is where you should start the book.  

- `.env`  
```
PROJECT_PATH=/path/to/where/you/downloaded/the/code/chapter_01/02
```

## Running the Project
There is a Taskfile provided that you can use to start up and shut down the development environment. Here are the commands.

`> task up`: This will build the Docker images and start up the containers using `docker-compose`.  
`> task down`: This will stop and remove all of the containers.  
`> task ruby-shell`: This will get you a shell on the Ruby/Rails container.  
`> task pg-shell`: This will get you a shell on the Postgres container.  


Based on the value of `PROJECT_PATH`, the code in that directory will be mounted into the Ruby/Rails container. This is the application code that will be executed when you run `task up`. Any changes made through your local editor on your laptop will automatically be reflected in the container, as it's using the same files.  

One thing of note, the files created by running the app in the container will be owned by the user of the container instead of by your system user. Just something to be aware of in case you decide you want to run the application on your system instead of in containers. You will likely have some permissions issues.



